// Keep global variables that user/dev may want to easily change in RON files in config/.
// Do not keep them in src/ as they will force recompiling when modified.

use amethyst::{
    window::DisplayConfig,
};
use ron::error::SpannedError;
use serde::{Deserialize, Serialize};
use std::{
    fs,
    path::PathBuf,
};

fn load<T>(path: PathBuf) -> Result<T, SpannedError> where for<'a> T: Deserialize<'a> {
    let file_content = fs::read_to_string(path)?;
    ron::from_str(&file_content)
}

pub struct EngineConfig {
    pub display: DisplayConfig,
}

impl EngineConfig {
    pub fn new(root_path: PathBuf) -> Result<EngineConfig, SpannedError> {
        Ok(EngineConfig {
            display: load::<DisplayConfig>(root_path.join("display.ron"))?
        })
    }
}

#[derive(Default)]
pub struct GameConfig {
    pub field_of_view: FieldOfView,
}

impl GameConfig {
    pub fn new(root_path: PathBuf) -> Result<GameConfig, SpannedError> {
        Ok(GameConfig {
            field_of_view: load::<FieldOfView>(root_path.join("field_of_view.ron"))?,
        })
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FieldOfView {
    pub width: f32,
    pub height: f32,
}

impl Default for FieldOfView {
    fn default() -> Self {
        FieldOfView {
            width: 200.0,
            height: 150.0,
        }
    }
}