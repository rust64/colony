use amethyst::{
    prelude::*,
};
use vector2d::Vector2D;

use crate::config::GameConfig;
use crate::user_experience::{assets, camera};
use crate::domain::unit;

// core game struct
#[derive(Default)]
pub struct State {

}

// Trait used by Amethyst's state machine to start, stop, and update the game.
// Its behavior mostly cares about handling the exit signal cleanly, by just quitting the application directly from the current state.
impl SimpleState for State {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) -> () {
        let world = data.world;
        camera::initialise_camera(world);
        let sprite_renders = assets::SpriteRenders::new(world);

        world.register::<unit::Unit>(); // only needed if no system uses the component

        let worker = unit::Unit::new(
            {
                let config = world.read_resource::<GameConfig>();
                Vector2D {
                    x: config.field_of_view.width * 0.5,
                    y: config.field_of_view.height * 0.5,
                }
            },
            5.0,
            // TODO: get size from sprite_render
            Vector2D { x: 18.0, y: 23.0 },
            sprite_renders.worker,
        );
        worker.spawn(world);
    }

    fn update(&mut self, _data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        Trans::None
    }
}