// Amethyst imports
use amethyst::{
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    core::transform::TransformBundle,
    utils::application_root_dir,
};

// Internal imports
// We must declare all modules used in our project even if they are not used in this file.
// The compiler starts here to explore our project.
// Imported modules can have their own imports but they will remain internal to that module.
mod config;
mod state;
mod domain;
mod user_experience;

fn main() -> amethyst::Result<()> {
	// Start Amethyst logger so we can see errors, warnings and debug messages while the program is running.
	// We could also configure it to write to a file.
	amethyst::start_logger(Default::default());

	// Load config
    let app_root = application_root_dir()?;
	let engine_config = config::EngineConfig::new(app_root.join("config").join("engine"))?;

    // GameDataBuilder is a central repository of all the game logic that runs periodically during the game runtime
	let game_data = GameDataBuilder::default()
    .with_bundle(
        RenderingBundle::<DefaultBackend>::new()
            // The RenderToWindow plugin provides all the scaffolding for opening a window and drawing on it
            .with_plugin(
                RenderToWindow::from_config(engine_config.display)
                    // Black background. Values range from 0.0 to 1.0
                    .with_clear([0.0, 0.0, 0.0, 1.0]),
            )
            // RenderFlat2D plugin is used to render entities with a `SpriteRender` component.
            .with_plugin(RenderFlat2D::default()),
    )?
    .with_bundle(TransformBundle::new())?;

	let assets_dir = app_root.join("assets");
    // Load game config
	let game_config = config::GameConfig::new(app_root.join("config").join("game"))?;
	// Application binds the OS event loop, state machines, timers and other core components in a central place
    // Use Application::build instead of ::new to add resources.
	let mut app = Application::build(
        assets_dir, 
        state::State::default(),
    )?
    .with_resource(game_config)
    .build(game_data)?;
	app.run();

	// Return value
    Ok(())
}

