use crate::domain::unit::Unit;
use vector2d::Vector2D;

pub trait Task: Sync + Send {
    fn start(&mut self, u: &Unit) -> ();
    fn execute(&mut self, u: &mut Unit) -> ();
    fn is_done(&self, u: &Unit) -> bool;
    fn boxed(&self) -> Box<dyn Task>;
}


#[derive(Clone)]
pub struct GoTo {
    destination: Vector2D<f32>,
    step: Option<Vector2D<f32>>
}

impl Task for GoTo {
    fn boxed(&self) -> Box<dyn Task> {
        Box::new(self.clone())
    }
    
    fn start(&mut self, unit: &Unit) -> () {
        self.step = Some((self.destination - unit.position).normalise() * unit.speed);
    }

    fn execute(&mut self, unit: &mut Unit) {
        if let Some(step) = self.step {
            unit.position += step;
        }
    }

    fn is_done(&self, unit: &Unit) -> bool {
        (self.destination - unit.position).length_squared() < 1.0
    }
}