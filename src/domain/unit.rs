use amethyst::{
    renderer::{SpriteRender, Transparent},
    core::transform::Transform,
    prelude::*,
    ecs::{Component, DenseVecStorage},
};
use vector2d::Vector2D;

use super::task::Task;

pub struct Unit {
    // Position -> sprite center
    pub position: Vector2D<f32>,
    pub speed: f32,
    // Sprite size
    size: Vector2D<f32>,
    sprite_render: SpriteRender,
    tasks: Vec<Box<dyn Task>>,
}

// By implementing Component for the Paddle struct, it can now be attached to entities in the game.
impl Component for Unit {
    type Storage = DenseVecStorage<Self>;
}

impl Unit {
    pub fn new(
        position: Vector2D<f32>,
        speed: f32,
        size: Vector2D<f32>,
        sprite_render: SpriteRender,
    ) -> Unit {
        Unit {
            position,
            speed,
            size,
            sprite_render,
            tasks: Vec::new(),
        }
    }

    pub fn spawn(
        &self,
        world: &mut World,
    ) {
        let mut sprite_transform = Transform::default();
        sprite_transform.set_translation_xyz(self.position.x, self.position.y, 0.);

        world
            .create_entity()
            .with(self.sprite_render.clone())
            .with(sprite_transform)
            .with(Transparent)
            .build();
    }

    pub fn add_task(&mut self, task: &dyn Task) {
        self.tasks.push(task.boxed())
    }
}