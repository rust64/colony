use amethyst::{
    core::Transform,
    prelude::*,
    renderer::Camera,
};
use crate::config::GameConfig;

pub fn initialise_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    
    let config = world.read_resource::<GameConfig>();
    transform.set_translation_xyz(config.field_of_view.width * 0.5, config.field_of_view.height * 0.5, 1.0);
    let camera2d = Camera::standard_2d(config.field_of_view.width, config.field_of_view.height);
    drop(config);

    world
        .create_entity()
        .with(camera2d)
        .with(transform)
        .build();
}