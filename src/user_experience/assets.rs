use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    prelude::{World, WorldExt},
    renderer::{ImageFormat, SpriteSheet, SpriteSheetFormat, Texture, SpriteRender},
};

pub struct SpriteRenders {
    pub worker: SpriteRender,
}

impl SpriteRenders {
    pub fn new(world: &mut World) -> SpriteRenders {
        let worker_sheet = load_sprite_sheet(world, "texture/link_px.png", "texture/link.ron");
        SpriteRenders {
            worker: SpriteRender::new(worker_sheet, 0),
        }
    }
}

// Loaded png file contains paddle and ball. Separation is made using the RON config file.
fn load_sprite_sheet(world: &mut World, texture_file: &str, config_file: &str) -> Handle<SpriteSheet> {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            texture_file,
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        config_file,
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}