# colony

Goal: colony builder, possibly zero-player-game or with AI, with gatherable resources, worker entities, storage sites, caravans and fighters.

# Roadmap

IMPORTANT NOTE: since Amethyst has been abandoned in favor of Bevy, we will eventually need to switch to it. However Bevy docs and tutorials are still incomplete. Let's move when they get better and we can easily understand Bevy.

Each version can be considered `Done` once bulletpoints are completed AND:
- `cargo run` does not fail or warn
- unit tests are written and pass
- documentation is written

## version 0.1.0
- [x] repo is created with both gitlab and cargo init content

## version 0.2.0
amethyst skeleton:
- [x] main.rs runs a `game`
- [x] engine config is loaded from RON files
- [x] state.rs contains an `on_start`
- [x] no systems, components or user_experience
- [x] window opens with black screen

## version 0.3.0
- [x] `Unit` struct is created
- [x] a first `Unit` instance is displayed on the screen with a sprite

## version 0.3.1
- [ ] fix bug that panics/exits when mouse enters window: bug is linked to amethyst, not us :/

## version 0.4.0
- [ ] `Unit` can move to a destination at a given speed